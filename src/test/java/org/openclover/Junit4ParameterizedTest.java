package org.openclover;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class Junit4ParameterizedTest {

	private int a, b, expected;

	public Junit4ParameterizedTest(int a, int b, int expected) {
		this.a = a;
		this.b = b;
		this.expected = expected;
	}

	@Test
	public void addTest() {
		assertEquals("Addition value is incorrect.", expected, Calculator.add(a, b));
	}

	@Parameters(name = "{index}: Sum of {0} and {1} = {2}")
	public static Collection<Object[]> parameters() {
		return Arrays.asList(new Object[][] { { 2, 3, 5 }, { 20, 40, 60 }, { 11, -11, 0 } });
	}

}
