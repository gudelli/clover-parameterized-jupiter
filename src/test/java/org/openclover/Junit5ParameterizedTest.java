package org.openclover;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class Junit5ParameterizedTest {

	@ParameterizedTest
	@MethodSource("parameterProvider")
	public void addTest(int a, int b, int expected) {
		assertEquals(expected, Calculator.add(a, b), "Addition value is incorrect.");
	}

	static Stream<Arguments> parameterProvider() {
		return Stream.of(Arguments.arguments(2, 3, 5), Arguments.arguments(20, 40, 60),
				Arguments.arguments(11, -11, 0));
	}
}
