 

var Packages = {
    nodes: [
                                                                                                        
                
{
    "id": "org.openclover",
    "text": "org.openclover",
    "package": "org.openclover",
    "url": "org/openclover/pkg-summary.html",
            "coverage": "100%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "org/openclover/pkg-summary.html"},
    "children": [
                    ]
},
            ],
    settings: {
        "icons": {
            "package": {
                "open": "aui-icon aui-icon-small aui-iconfont-devtools-folder-open",
                "closed": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed"
            },
            "state": {
                "collapsed": "aui-icon aui-icon-small aui-iconfont-collapsed",
                "expanded": "aui-icon aui-icon-small aui-iconfont-expanded",
                "forRemoval": "hidden aui-iconfont-collapsed aui-iconfont-expanded"
            }
        }
    }
};
