# README #



### What is this repository for? ###

* A sample project to demonstrate an issue with clover test results when the project is set up to collect both maven-surefire-plugin and maven-failsafe-plugin tests (combined report) as documented in http://openclover.org/doc/manual/latest/maven--using-with-surefire-and-failsafe-plugins.html. 

* It seems like the issue is with using clover-report.xml but not because of using Junit 5. 
